import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-saydbar',
  templateUrl: './saydbar.component.html',
  styleUrls: ['./saydbar.component.css']
})
export class SaydbarComponent  {
  @Input() title = "home";
  @Input() link = "#"
}
