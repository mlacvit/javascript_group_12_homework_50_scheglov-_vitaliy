import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaydbarComponent } from './saydbar.component';

describe('SaydbarComponent', () => {
  let component: SaydbarComponent;
  let fixture: ComponentFixture<SaydbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaydbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaydbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
