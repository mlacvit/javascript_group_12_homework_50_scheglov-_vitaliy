import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomeWork50Component } from './home-work50/home-work50.component';
import { FilmsComponent } from './films/films.component';
import { HeaderComponent } from './header/header.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { SaydbarComponent } from './saydbar/saydbar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeWork50Component,
    FilmsComponent,
    HeaderComponent,
    ContentComponent,
    FooterComponent,
    SaydbarComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
