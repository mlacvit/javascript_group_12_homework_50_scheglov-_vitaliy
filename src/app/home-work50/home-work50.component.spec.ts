import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeWork50Component } from './home-work50.component';

describe('HomeWork50Component', () => {
  let component: HomeWork50Component;
  let fixture: ComponentFixture<HomeWork50Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomeWork50Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeWork50Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
