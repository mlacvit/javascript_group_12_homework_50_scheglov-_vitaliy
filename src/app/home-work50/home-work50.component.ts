import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-home-work50',
  templateUrl: './home-work50.component.html',
  styleUrls: ['./home-work50.component.css']
})
export class HomeWork50Component  {
  @Input() name = '';
  @Input() year = 0;
  @Input() poster = "";
}
