import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
@Input() logo = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxbrjUfUVuUorPZCpkItIU3N27_5UCtBDoOh637QmFh3lZ5q9DknPw4qnDLkiiyTzqOiI&usqp=CAU";
}
